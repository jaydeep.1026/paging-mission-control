/**
 * 
 */
package app;

import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import model.Alert;
import model.TelemetryData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Queue;
import java.util.LinkedList;
import java.util.List;
import model.TelemetryData.Component;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author jayde
 *
 */
public class PagingMissionControl {

	private static Map<Integer, Map<Component, Queue<TelemetryData>>> LAST_TIMESTAMPS = new HashMap<>();
	private static List<Alert> ALERTS = new ArrayList<>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Consider only valid input file name
		if (args != null && args.length > 0 && args[0] != null && args[0].length() > 0) {
			String filename = args[0];
			// Prep output file
			String outputfilename = filename + "-" + "output";
			File outputfile = new File(outputfilename);
			if (outputfile.exists()) {
				outputfile.delete();
			}
			// Process input file
			process(filename, outputfilename);
		} else {
			System.out.println("Invalid File Name");
		}
	}

	// Read input
	public static void process(String filename, String outputfilename) {
		Scanner scanner = null;
		// Open file as a resource stream
		try {
			scanner = new Scanner(PagingMissionControl.class.getClassLoader().getResourceAsStream(filename));
		} catch (Exception e) {
			System.out.println("Error Opening/Streaming File as Resource");
			e.printStackTrace();
		}
		// Read and Process file
		if (scanner != null) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!line.trim().isEmpty()) {
					// Line representing telemetry data
					TelemetryData td = new TelemetryData(line);
					// Last time-stamps
					Map<Component, Queue<TelemetryData>> lts = LAST_TIMESTAMPS.getOrDefault(td.getSatelliteID(), new HashMap<Component, Queue<TelemetryData>>());
					if (lts.isEmpty()) {
						lts.put(Component.BATT, new LinkedList<TelemetryData>());
						lts.put(Component.TSTAT, new LinkedList<TelemetryData>());
					}
					Queue<TelemetryData> readings = lts.get(td.getComponent());
					// Remove telemetry data entries that are older than five minutes
					while(!readings.isEmpty() && isBeyondFiveMinutes(readings.peek().getTimeStamp(), td.getTimeStamp())) {
						@SuppressWarnings("unused")
						TelemetryData teledata = readings.poll();
					}
					// Offer new telemetry data entry if it violates the condition
					switch(td.getComponent()) {
						case BATT:
							if (td.getRawVal() < td.getRedLowLimit()) {
								readings.offer(td);
							}
							break;
						case TSTAT:
							if (td.getRawVal() > td.getRedHighLmt()) {
								readings.offer(td);
							}
							break;
						default:
							// Should not be reachable
					}
					// Create an alert
					if (readings.size() >= 3) {
						ALERTS = new ArrayList<>();
//						java.util.Iterator<TelemetryData> iterator = readings.iterator();
//						while (iterator.hasNext()) {
//							ALERTS.add(Alert.getInstance(iterator.next()));
//						}
						while(!readings.isEmpty()) {
							ALERTS.add(Alert.getInstance(readings.poll()));
						}
						FileOutputStream fos = null;
						try {
						    fos = new FileOutputStream(outputfilename);
							ObjectMapper mapper = new ObjectMapper();
						    byte[] bytes = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ALERTS).getBytes();
						    fos.write(bytes);
						} catch (JsonProcessingException e) {
							System.out.println("JSON processing error");
							e.printStackTrace();
						} catch (FileNotFoundException e) {
							System.out.println("File Not Found");
							e.printStackTrace();
						} catch (IOException e) {
							System.out.println("Error while writing to file: " + outputfilename);
							e.printStackTrace();
						} finally {
							if (fos != null) {
								try {
									fos.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
						System.out.println("==================================================");
						System.out.println("Satellite: " + td.getSatelliteID() + " Output of " + td.getComponent().name() + " Component");
						printOutput(outputfilename);
						System.out.println("==================================================");
					}
					LAST_TIMESTAMPS.put(td.getSatelliteID(), lts);
				}
			}
			scanner.close();
		}
	}

	private static boolean isBeyondFiveMinutes(Timestamp ts1, Timestamp ts2) {
	    long milliseconds = ts2.getTime() - ts1.getTime();
	    int seconds = (int) milliseconds / 1000;
	    int minutes = seconds / 60;
	    return minutes > 5;
	}

	// Print output
	private static void printOutput(String outputfilename) {
		Scanner scanner = null;
		File outputfile = new File(outputfilename);
		InputStream is = null;
		try {
			is = new FileInputStream(outputfile);
			scanner = new Scanner(is);
			while (scanner.hasNextLine()) {
				System.out.println(scanner.nextLine());
			}
		} catch (Exception e) {
			System.out.println("Error Opening/Reading File: " + outputfilename);
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (scanner != null) {
				scanner.close();
			}
			if (outputfile.exists()) {
				try {
					outputfile.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
