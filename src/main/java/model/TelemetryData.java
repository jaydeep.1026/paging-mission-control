package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat; 
import java.util.Date;
import java.io.Serializable;

public class TelemetryData implements Serializable {

	// <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
	// 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT

	/**
	 * 
	 */
	private static final long serialVersionUID = 6034510770629410619L;

	// enum
	public enum InputField {
		
		TS ("timestamp"),
		SAT_ID ("satellite-id"),
		RED_HIGH_LMT ("red-high-limit"),
		YEL_HIGH_LMT ("yellow-high-limit"),
		YEL_LOW_LMT ("yellow-low-limit"),
		RED_LOW_LMT ("red-low-limit"),
		RAW_VAL ("raw-value"),
		COM ("component");

		private String val;

		InputField(String val) {
			this.val = val;
		}

		public String value() {
			return val;
		}

	}
	
	public enum Component {
		TSTAT, BATT;	
	}

	private Component com;
	private Timestamp ts;
	private int satId;
	private int redLowLmt;
	private int redHighLmt;
	private int yelLowLmt;
	private int yelHighLmt;
	private float rawVal;
	
	@SuppressWarnings("unused")
	private TelemetryData() {}
	
	public TelemetryData (String input) {
		try {
			String[] data = input.split("\\|");
			this.com = Component.valueOf(data[InputField.COM.ordinal()]);
			// 20180101 23:01:05.001
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS");
			Date date = df.parse(data[InputField.TS.ordinal()]);
			this.ts = new Timestamp(date.getTime());
			this.satId = Integer.valueOf(data[InputField.SAT_ID.ordinal()]);
			this.redLowLmt = Integer.valueOf(data[InputField.RED_LOW_LMT.ordinal()]);
			this.redHighLmt = Integer.valueOf(data[InputField.RED_HIGH_LMT.ordinal()]);
			this.yelLowLmt = Integer.valueOf(data[InputField.YEL_LOW_LMT.ordinal()]);
			this.yelHighLmt = Integer.valueOf(data[InputField.YEL_HIGH_LMT.ordinal()]);
			this.rawVal = Float.valueOf(data[InputField.RAW_VAL.ordinal()]);
		} catch (Exception e) {
			System.out.println("Failed to instantiate Telemetry Data for input: " + input + " with following exception");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public Component getComponent() {
		return com;
	}

	public Timestamp getTimeStamp() {
		return ts;
	}
	
	public int getSatelliteID() {
		return satId;
	}
	
	public int getRedLowLimit() {
		return redLowLmt;
	}
	
	public int getRedHighLmt() {
		return redHighLmt;
	}
	
	public int getYellowLowLimit() {
		return yelLowLmt;
	}
	
	public int getYellowHighLimit() {
		return yelHighLmt;
	}
	
	public float getRawVal() {
		return rawVal;
	}

}
