package model;

import java.sql.Timestamp;
import model.TelemetryData.Component;
import java.io.Serializable;

public class Alert implements Serializable {

	/*
		{
			"satelliteId": 1000,
			"severity": "RED LOW",
			"component": "BATT",
			"timestamp": "2018-01-01T23:01:09.521Z"
		}
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 201391877425512021L;

	enum OutputField {

		SAT_ID ("satelliteId"),
		SEVERITY ("severity"),
		COM ("component"),
		TS ("timestamp");

		private String val;

		OutputField(String val) {
			this.val = val;
		}

		public String value() {
			return val;
		}
	}
	
	enum Severity {

		RED_LOW_LMT ("RED LOW"),
		RED_HIGH_LMT ("RED HIGH"),
		YEL_LOW_LMT ("YELLOW LOW"),
		YEL_HIGH_LMT ("YELLOW HIGH");

		private String val;

		Severity(String val) {
			this.val = val;
		}

		public String value() {
			return val;
		}

	}

	private int satId;
	private Severity severity;
	private Component com;
	private Timestamp ts;

	private Alert() {}
	
	private Alert(int satId, Component com, Timestamp ts) {
		this.satId = satId;
		this.com = com;
		this.severity = com == Component.BATT ? Severity.RED_LOW_LMT : Severity.RED_HIGH_LMT;
		this.ts = ts;
	}

	public static Alert getInstance (TelemetryData td) {
		return new Alert(td.getSatelliteID(), td.getComponent(), td.getTimeStamp());
	}

	public int getSatelliteID() {
		return satId;
	}
	
	public Severity getSeverity() {
		return severity;
	}

	public Component getComponent() {
		return com;
	}

	public Timestamp getTimeStamp() {
		return ts;
	}

//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		//String timestamp = new SimpleDateFormat("yyyy-MM-ddThh:mm:ss.SSSZ").format(getTimeStamp());
//		sb.append(" ").append("{").append(System.lineSeparator());
//		sb.append(" ").append(" ").append(OutputField.SAT_ID.value()).append(": ").append(getSatelliteID()).append(System.lineSeparator());
//		sb.append(" ").append(" ").append(OutputField.SEVERITY.value()).append(": ").append(getSeverity().value()).append(System.lineSeparator());
//		sb.append(" ").append(" ").append(OutputField.COM.value()).append(": ").append(getComponent().toString()).append(System.lineSeparator());
//		sb.append(" ").append(" ").append(OutputField.TS.value()).append(": ").append(getTimeStamp().toInstant()).append(System.lineSeparator());
//		sb.append(" ").append("}");
//		return sb.toString();
//	}
//	
//	public static String toString(java.util.List<Alert> alerts) {
//		StringBuilder sb = new StringBuilder();
//		sb.append("[");
//		sb.append(System.lineSeparator());
//		if (alerts != null) {
//			int i = 0;
//			for (Alert alert : alerts) {
//				if (alert != null) {
//					sb.append(alert.toString());
//				}
//				if (i < (alerts.size() - 1)) {
//					sb.append(", ");
//				}
//				sb.append(System.lineSeparator());
//				i++;
//			}
//		}
//		sb.append("]");
//		return sb.toString();
//	}

}
